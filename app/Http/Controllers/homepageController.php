<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;
use App\Profile;
use App\Skill;
use App\Service;
use App\Statistic;
use App\Project;
use App\Testimonial;


class homepageController extends Controller
{
    public function index()
    {
        $banners = Banner::all();
        $profiles = Profile::all();
        $skills = Skill::all();
        $services = Service::all();
        $statistics= Statistic::all();
        $projects= Project::all();
        $testimonials= Testimonial::all();
        //dd($services);
        return view('homepage', compact('banners','profiles','skills','services','statistics','projects','testimonials'));
    }
}

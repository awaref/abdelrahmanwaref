@extends('master')

@section('content')


<!--Banner area starts-->

<div class="banner-area" id="home">
    <div id="particles-js"></div>
    <div class="banner-table">
        <div class="banner-table-cell">
            <div class="welcome-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <section class="intro animate-scale">

                                <h3>Hello, I'm</h3>

                                <h1 class="ah-headline">

                                    <span class="ah-words-wrapper">
                                        
                                        <b class="is-visible">Abdelrahman Waref</b> <!--edit the name to your name-->
                                       @foreach ($banners as $banner)

                                    <b> {{$banner->text}}</b>
                                           
                                       @endforeach
                                        
                                     
                                    </span>
                                </h1>

                                <a href="#contact" class="banner-btn">Contact me</a>


                            </section>

                            <div class="clearfix"></div>

                            <a class="mouse-scroll hidden-xs" href="#about">
                                <span class="mouse">
                                    <span class="mouse-movement"></span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Banner area ends-->

<!--about area starts-->
@foreach ($profiles as $profile)
    


<div class="about-area section-padding" id="about">
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-xs-12 col-sm-6 col-lg-4">
                <div class="about-text-left">
                    <h2>{{$profile->name}}</h2> <!--edit name-->
                    <h3>{{$profile->profession}}</h3> <!--edit designation-->
                    <p>{{$profile->description}} </p>
                    <a href="/storage/{{ json_decode($profile->cv)[0]->download_link}}" download>Download CV &nbsp; &nbsp;<i class="fa fa-download"></i></a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                <img src="{{ Voyager::image( $profile->image )}}"  class="img-responsive" alt="about image"> <!--add your image here-->
            </div>
            @endforeach

            <div class="col-md-4 col-xs-12 col-sm-12 col-lg-4">
                <div class="about-text-right">

                    <div id="skills">

                        <h3>My Skills</h3>
                        @foreach ($skills as $skill)

                        <div class="row">

                            <div class="col-md-12">
                              
                                
                                <!-- skillbar -->
                                <div class="skillbar" data-percent="{{$skill->percentage}}%"> <!--edit percentage-->

                                    <h6 class="skillbar-title">{{$skill->title}}</h6> <!--edit skills-->
                                    <h6 class="skillbar-percent">{{$skill->percentage}}%</h6> <!--edit percentage-->

                                    <div class="skillbar-bar">
                                        <div class="skillbar-child"></div>
                                    </div>

                                  

                                </div>
                                <!-- end skillbar -->

                            </div>
                          


                            </div>
                           

                         @endforeach
                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>
</div>

<!--about area ends-->

<!--Services Area Starts-->

<div id="services" class="services-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header wow fadeInDown" data-wow-delay="0.2s">
                    <p class="line-one"></p>
                    <h2>What I Offer</h2>
                    <p class="line-one"></p>

                </div>
            </div>
        </div>
        <div class="row">
            <div id="services-carousel" class="owl-carousel owl-theme">
                @foreach ($services as $service)
                    
               
                <div class="single-services text-center item">
                    <div class="services-icon">
                        <i class="fa fa-joomla"></i>
                    </div>
                    <div class="services-content">
                        <h3>{{$service->title}}</h3> <!--edit the service you give-->
                        <p>{{$service->description}}</p>
                    </div>

                </div>

                @endforeach

            
            </div>

        </div>
    </div>
</div>

<!--Services Area Ends-->

<!--number area starts-->

<div class="number-area section-padding">
    <div class="container">
        <div class="row">
            @foreach ($statistics as $statistic)
                
           
            <div class="col-md-3 col-xs-6">
                <div class="single-number text-center">
                    <i class="{{$statistic->icon}}"></i>
                    <h2 class="counter">{{$statistic->number}}</h2> <!--edit-->
                    <p>{{$statistic->title}}</p>
                </div>
            </div>
            @endforeach
         
        </div>
    </div>
</div>

<!--number area ends-->

<!--Portfolio Area Starts-->

<div id="portfolio" class="portfolio-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header wow fadeInDown" data-wow-delay="0.2s">
                    <p class="line-one"></p>
                    <h2>My Works</h2>
                    <p class="line-one"></p>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="portfolio-items">
                @foreach ($projects as $project)
                    
                

                <div class="col-md-4 col-sm-6 col-xs-12 no-pad">
                    <div id="inline-popups" class="port-box">
                    <a href="#test-popup-{{$project->id}}" data-effect="mfp-zoom-out">
                            <div class="hovereffect">

                                <img src="{{Voyager::image( $project->image)}}" alt="portfolio image" class="img-responsive"> <!--edit image-->
                                <div class="overlay">
                                    <h2>{{$project->title}}</h2> <!--your project name-->
                                <p>{{$project->description}}</p>
                                </div>

                            </div>
                        </a>
                    </div>
                 
                   

                <div id="test-popup-{{$project->id}}" class="white-popup mfp-with-anim mfp-hide">
                        <div class="row">
                            <div class="col-md-7 col-xs-12">
                                <div class="por-img">
                                    <img src="{{Voyager::image( $project->image)}}" alt="portfolio image" class="img-responsive"> <!--edit image-->
                                </div>
                            </div>
                            <div class="col-md-5 col-xs-12">
                                <div class="por-text">
                                    <h2>{{$project->title}}</h2> <!--your project title-->
                                    <p>{{$project->description}}</p>
                                    <div class="por-text-details">
                                        <div class="row">
                                            <div class="col-xs-4">
                                                <p>Client: </p>
                                                
                                                <p>Type:</p>
                                                <p>link:</p>
                                            </div>
                                            <div class="col-xs-offset-1 col-xs-7">
                                                <p>{{$project->client}}</p> <!--edit here-->
                                                
                                                <p>{{$project->type}}</p> <!--edit here-->
                                                <p>{{$project->link}}</p> <!--edit here-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                @endforeach

              

              

            </div> <!--end portfolio grid -->

        </div>


    </div>
</div>


<!--Portfolio Area Ends-->

<!--Testimonial Section Starts-->

<div id="testimonial" class="testimonial-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header wow fadeInDown" data-wow-delay="0.2s">
                    <p class="line-one"></p>
                    <h2>What Clients Say</h2>
                    <p class="line-one"></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="review-area">
                    <div id="testimonial-carousel" class="owl-carousel owl-theme">
                        @foreach ($testimonials as $testimonial)
                            
                       
                        <div class="single-testi text-center item">
                            <div class="testi-img">
                                <img src="{{Voyager::image( $testimonial->image)}}" alt="testimonial image"> <!--edit image-->
                            </div>
                            <div class="block-quote">
                                <p>{{$testimonial->description}}</p> <!--edit here-->
                                <h2>{{$testimonial->name}}</h2> <!--edit here-->
                                <h3>{{$testimonial->position}}</h3> <!--edit here-->
                            </div>
                        </div>
                        @endforeach
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Testimonial Section Ends-->

<!--contact area starts-->

<div class="contact-area section-padding" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header wow fadeInDown" data-wow-delay="0.2s">
                    <p class="line-one"></p>
                    <h2>Contact</h2>
                    <p class="line-one"></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-contact text-center wow fadeInDown" data-wow-delay="0.4s">
                    <i class="fa fa-home"></i>
                    <h2>Location</h2>
                    <p>New Cairo, Egypt</p> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-contact text-center wow fadeInDown" data-wow-delay="0.6s">
                    <i class="fa fa-phone"></i>
                    <h2>Phone: </h2>
                    <p>(+20) 1007439676</p> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-contact text-center wow fadeInDown" data-wow-delay="0.8s">
                    <i class="fa fa-envelope-o"></i>
                    <h2>Email</h2>
                    <p>abdelrahmanwaref@gmail.com</p> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-contact text-center wow fadeInDown" data-wow-delay="1s">
                    <i class="fa fa-gg"></i>
                    <h2>Social Media: </h2>
                    <div class="socials">
                        <a href="https://www.facebook.com/abdelrahman.waref" target="_blank"><i class="fa fa-facebook"></i></a> <!--your facebook profile link here-->
                        <a href="https://www.linkedin.com/in/abdelrahman-waref" target="_blank"><i class="fa fa-linkedin"></i></a> <!--your linkedin profile link here-->
                      
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-10 col-md-offset-1">
                <form id="contact-form" method="post" action="contact.php" class="wow fadeInDown" data-wow-delay="1.2s">

                    <div class="messages"></div> <!--you can change the message in contact.php file -->

                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Enter your full name *" required="required" data-error="Fullname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Enter your email *" required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea id="form_message" name="message" class="form-control" placeholder="Your Message *" rows="4" required="required" data-error="Leave a message for me"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-send" value="">Send message</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

        </div>
    </div>
</div>

<!--contact area ends-->



    
@endsection
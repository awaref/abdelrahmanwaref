<!doctype html>
<html lang="en">
<head>
    <!--meta tags-->
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="portfolio template based on HTML5">
    <meta name="keywords" content="onepage, developer, resume, cv, personal, portfolio, personal resume, clean, modern">
    <meta name="author" content="MouriTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--template title-->
    <title>Abdelrahman Waref - Personal Website</title>

    <!--==========Favicon==========-->

    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!--========== Theme Fonts ==========-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,600,700,800" rel="stylesheet">

    <!--Font Awesome css-->
    <link rel="stylesheet" href="frontend/css/font-awesome.min.css">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="frontend/css/bootstrap.min.css">

    <!--Animated headline css-->
    <link rel="stylesheet" href="frontend/css/jquery.animatedheadline.css">

    <!--Animate css-->
    <link rel="stylesheet" href="frontend/css/animate.css">
    
	<!--Owl carousel css-->
	<link rel="stylesheet" href="frontend/css/owl.carousel.css">
	<link rel="stylesheet" href="frontend/css/owl.theme.default.css">
    
	<!--Magnific popup css-->
	<link rel="stylesheet" href="frontend/css/magnific-popup.css">
    
	<!--Normalizer css-->
	<link rel="stylesheet" href="frontend/css/normalize.css">

    <!--Theme css-->
    <link rel="stylesheet" href="frontend/css/style.css">

    <!--Responsive css-->
    <link rel="stylesheet" href="frontend/css/responsive.css">
</head>
<body>

    <!--preloader starts-->


    <div class="loader_bg"><div class="loader"></div></div>


    <!--preloader ends-->

    <!--navigation area starts-->

    <header class="nav-area navbar-fixed-top">
        <div class="container">
            <div class="row">
                <!--main menu starts-->

                <div class="col-md-12">
                    <div class="main-menu">
                        <div class="navbar navbar-cus">
                            <div class="navbar-header">
                                <a href="index.html" class="navbar-brand"><span class="logo">a.waref</span></a> <!--edit site name here-->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse">
                                <nav>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="smooth-menu" href="#home">Home</a></li>
                                        <li><a class="smooth-menu" href="#about">About</a></li>
                                        <li><a class="smooth-menu" href="#services">Services</a></li>
                                        <li><a class="smooth-menu" href="#portfolio">Portfolio</a></li>
                                        <li><a class="smooth-menu" href="#testimonial">Testimonial</a></li>
                                        <li><a class="smooth-menu" href="#contact">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                <!--main menu ends-->
            </div>
        </div>
    </header>

    <!--navigation area ends-->


    @yield('content')




     <!--Footer Area Starts-->

     <div class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <p>&copy; All Right Reserved By <a href="#" target="_blank">Abdelrahman Waref</a></p> <!--edit here-->
                </div>
            </div>
        </div>
    </div>

    <!--Footer Area Ends-->
    
    



    <!--Latest version JQuery-->
    <script src="frontend/js/jquery-3.2.1.min.js"></script>

    <!--Bootstrap js-->
    <script src="frontend/js/bootstrap.min.js"></script>

	<!--Magnific popup js-->
	<script src="frontend/js/jquery.magnific-popup.js"></script>

	<!--Owl Carousel js-->
	<script src="frontend/js/owl.carousel.js"></script>

    <!--Animated headline js-->
    <script src="frontend/js/jquery.animatedheadline.js"></script>

    <!--wow js-->
    <script src="frontend/js/wow.min.js"></script>
    
    <!--Validator js-->
    <script src="frontend/js/jquery.waypoints.js"></script>
    
	<!--counter up js-->
	<script src="frontend/js/jquery.counterup.js"></script>

    <!--particles js-->
    <script src="frontend/js/particles.js"></script>
    <script src="frontend/js/app.js"></script>

    <!--Validator js-->
    <script src="frontend/js/validator.js"></script>

    <!--Contact js-->
    <script src="frontend/js/contact.js"></script>

    <!--Main js-->
    <script src="frontend/js/main.js"></script>
    
    <script>

	</script>
</body>
</html>